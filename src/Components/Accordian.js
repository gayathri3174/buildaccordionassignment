import React, { useState } from 'react'

function Accordian() {

    const [openIndex, setOpenIndex] = useState(null)
    const data = [
        {
            title: "Product 1 ",
            content: "content for product 1"
        }, {
            title: "product 2",
            content: "content for section 2 Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC"
        }, {
            title: "product 3",
            content: "content for section 2 Lorem Ipsum is not simply random text. I"
        },
        {
            title: "product 4",
            content: "content for section 2 Lorem Ipsum is not simply random text. I"
        }
    ]

    const handleAccordian = (index) =>{
       setOpenIndex(openIndex === index ? null : index)

    }
    const accordiansection = {
       textAlign:"center",display:"flex",alignItems:"center",justifyContent:"center",flexDirection:"column",border:"1px solid black",width:"80vh",margin:"4rem auto",padding:"1rem",borderRadius:"0.5rem",boxShadow:"10px 5px  20px gray"
        
    }

    const accordianTitle = {
        border:"2px solid black",width:"500px",color:"blue",fontFamily:"cursive"
    }
    const accordianhead = {
        fontFamily:"sans-serif",color:"black",backgroundColor:"gray",padding:"1rem",borderRadius:"1rem"
    }
    return (
        <div style={accordiansection}>
            <h1 style={accordianTitle}>Accordian Assignment</h1>
            {data.map((eachsection, index) => (
                <div style={{cursor:'pointer'}} >
                   <div onClick={()=>{handleAccordian(index)}} style={{margin:"1rem" }} >
                   <h1 style={accordianhead}>{eachsection.title}   <span style={{fontSize:"1rem"}}> view content</span></h1>
                   {openIndex === index && (
                        <div>
                           <p>{eachsection.content}</p>
                        </div>
                    )}
                    </div>
                    
                </div>
            ))}

        </div>
    )
}

export default Accordian